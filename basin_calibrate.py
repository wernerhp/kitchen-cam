import array
import sys
import cv2
import numpy as np
from matplotlib import pyplot as plt

#get template file name
temp_out_filename = 'basin_template.jpg'
if len(sys.argv) > 1:
    temp_out_filename = sys.argv[1]

img = cv2.imread('basin.jpg',0)
img2 = img.copy()
template = cv2.imread('basin_template.jpg',0)
w, h = template.shape[::-1]

# All the 6 methods for comparison in a list
#methods = ['cv2.TM_CCOEFF', 'cv2.TM_CCOEFF_NORMED', 'cv2.TM_CCORR',
#            'cv2.TM_CCORR_NORMED', 'cv2.TM_SQDIFF', 'cv2.TM_SQDIFF_NORMED']

#methods = ['cv2.TM_CCOEFF']
methods = ['cv2.TM_CCOEFF_NORMED']

for meth in methods:
    img = img2.copy()
    method = eval(meth)

    # Apply template Matching
    res = cv2.matchTemplate(img,template,method)
    min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(res)

    # If the method is TM_SQDIFF or TM_SQDIFF_NORMED, take minimum
    if method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
        top_left = min_loc
    else:
        top_left = max_loc
    bottom_right = (top_left[0] + w, top_left[1] + h)

    cv2.rectangle(img,top_left, bottom_right, 255, 2)

    plt.subplot(121),plt.imshow(res,cmap = 'gray')
    plt.title('Matching Result'), plt.xticks([]), plt.yticks([])
    plt.subplot(122),plt.imshow(img,cmap = 'gray')
    plt.title('Detected Point'), plt.xticks([]), plt.yticks([])
    plt.suptitle(meth)

    #plt.show()

    #cropped_image = img[top_left[0]:bottom_right[0] , top_left[1]:bottom_right[1]].copy()

    cropped_image = img[top_left[1]:bottom_right[1] , top_left[0]:bottom_right[0]].copy()

    rectangle_crop = [ top_left[0], top_left[1], bottom_right[0], bottom_right[1] ]

    #save coord of the calibration
    print rectangle_crop
    np.savetxt('crop.csv', rectangle_crop, delimiter=",")

    cv2.imwrite( temp_out_filename, cropped_image)
    cv2.imshow('result',cropped_image)
    cv2.waitKey()
