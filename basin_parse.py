import requests
import subprocess as sub
import subprocess
import os
import math, operator
from PIL import Image
import functools
import time
import hipchat
import numpy

hipster = hipchat.HipChat(token='')
dishdetected = False
hipchat_posted = False
while True:

    #get file
    with open('basin.jpg', 'wb') as handle:
        response = 0 
        try:
            response = requests.get('http://192.168.10.152:8080/photoaf.jpg', stream=True)
            if not response.ok:
                print("could not download basin image...")

            for block in response.iter_content(1024):
                if not block:
                    break
                handle.write(block)
        except Exception:
            pass



    print('download done, cropping')

    if os.stat('basin.jpg').st_size == 0:
        print("no image found")
        time.sleep(5)
        continue #try again

    test_image = 'basin.jpg'
    original = Image.open(test_image)
    #original.show()

    width, height = original.size   # Get dimensions

    #load calibrated position from file
    crop_rect = numpy.loadtxt('crop.csv',delimiter=",")
    crop_rect = crop_rect.astype(int)
    print(crop_rect)
    left = crop_rect[0]
    top = crop_rect[1]
    right = crop_rect[2]
    bottom = crop_rect[3]

    cropped_example = original.crop((left, top, right, bottom))
    #cropped_example.show()
    cropped_example.save('basin_cropped.jpg')

    h1 = cropped_example.histogram()
    h2 = Image.open("basin_template.jpg").histogram() # fetch template

    rms = math.sqrt(functools.reduce(operator.add,
            map(lambda a,b: (a-b)**2, h1, h2))/len(h1))

    print("difference detected = ",rms)

    template_counter = 0
    if ( rms > 110 and rms < 175):
        print("dish detected...")
        #subprocess.getoutput('/usr/bin/notify-send "possible dish in basin"')
        #hipster.message_room('Sandbox', 'Kitchen Cam', 'Possible dish detected. Diff factor = '+str(rms)+'. http://192.168.10.152:8080/photo.jpg' )
        if not hipchat_posted:
            hipster.message_room('KitchenCam', 'Kitchen Cam', 'Possible dish detected. Diff factor = '+str(rms)+'. http://192.168.10.152:8080/photo.jpg')
            hipchat_posted = True
        dishdetected = True
    else: #test save as new template?
        if hipchat_posted and dishdetected:
            hipster.message_room('KitchenCam', 'Kitchen Cam', 'Dish has been removed. http://192.168.10.152:8080/photo.jpg' )
            hipchat_posted = False
        dishdetected = False

    time.sleep(20)
